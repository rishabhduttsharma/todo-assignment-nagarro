package com.nagarro.assignment.di.module

import android.app.Application
import com.nagarro.assignment.TodoApplication
import dagger.Module
import dagger.Provides
import javax.inject.Qualifier

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
@Module
class AppModule(private val application: TodoApplication) {

    @ApplicationContext
    @Provides
    fun provideApplication(): Application = application
}

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext