package com.nagarro.assignment.di.module

import com.nagarro.assignment.data.source.remote.api.TodoApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
@Module(includes = [NetworkModule::class])
object ApiModule {

    @Provides
    fun provideTodoApi(retrofit: Retrofit) = retrofit.createApi<TodoApi>()

    private inline fun <reified T> Retrofit.createApi() = create(T::class.java)
}