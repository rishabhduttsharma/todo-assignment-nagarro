package com.nagarro.assignment.di

import com.nagarro.assignment.TodoApplication
import com.nagarro.assignment.di.module.TodoDataModule
import com.nagarro.assignment.di.module.TodoUiModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        TodoUiModule::class,
        TodoDataModule::class
    ]
)
interface TodoAppComponent : AndroidInjector<TodoApplication>