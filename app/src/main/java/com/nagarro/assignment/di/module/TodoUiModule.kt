package com.nagarro.assignment.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nagarro.assignment.ui.todolist.TodoListActivity
import com.nagarro.assignment.ui.todolist.TodoListFragment
import com.nagarro.assignment.ui.todolist.TodoListViewModel
import com.nagarro.assignment.util.viewmodel.DaggerViewModelFactory
import com.nagarro.assignment.util.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/14/2020
 */
@Module
abstract class TodoUiModule {

    @ContributesAndroidInjector
    abstract fun contributesTodoListActivity(): TodoListActivity

    @ContributesAndroidInjector
    abstract fun contributesTodoListFragment(): TodoListFragment

    @Binds
    abstract fun bindsDaggerViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(TodoListViewModel::class)
    abstract fun bindsTodoListViewModel(viewModel: TodoListViewModel): ViewModel
}