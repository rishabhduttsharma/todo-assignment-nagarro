package com.nagarro.assignment.di.module

import com.nagarro.assignment.data.source.ITodoDataRepository
import com.nagarro.assignment.data.source.TodoDataRepository
import com.nagarro.assignment.data.source.local.TodoDatabase
import com.nagarro.assignment.data.source.local.TodoLocalDataSource
import com.nagarro.assignment.data.source.remote.TodoRemoteDataSource
import com.nagarro.assignment.data.source.remote.api.TodoApi
import dagger.Module
import dagger.Provides

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
@Module(includes = [ApiModule::class, DatabaseModule::class])
object TodoDataModule {

    @Provides
    fun provideTodoLocalDataSource(todoDatabase: TodoDatabase): TodoLocalDataSource =
        TodoLocalDataSource(todoDatabase.getTodoDao())

    @Provides
    fun provideTodoRemoteDataSource(api: TodoApi): TodoRemoteDataSource =
        TodoRemoteDataSource(api)

    @Provides
    fun provideTodoDataRepository(
        localDataSource: TodoLocalDataSource,
        remoteDataSource: TodoRemoteDataSource
    ): ITodoDataRepository = TodoDataRepository(localDataSource, remoteDataSource)
}