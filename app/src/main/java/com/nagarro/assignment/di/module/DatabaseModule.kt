package com.nagarro.assignment.di.module

import android.app.Application
import androidx.room.Room
import com.nagarro.assignment.data.AppConstant
import com.nagarro.assignment.data.source.local.TodoDatabase
import dagger.Module
import dagger.Provides

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
@Module(includes = [AppModule::class])
class DatabaseModule {

    @Provides
    fun provideRoomDatabase(@ApplicationContext application: Application): TodoDatabase =
        Room.databaseBuilder(
            application, TodoDatabase::class.java, AppConstant.RoomDatabaseName.TODO
        ).fallbackToDestructiveMigration().build()
}