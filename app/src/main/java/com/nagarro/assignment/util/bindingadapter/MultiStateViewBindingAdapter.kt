package com.nagarro.assignment.util.bindingadapter

import android.widget.Button
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.kennyc.view.MultiStateView
import com.kennyc.view.MultiStateView.ViewState

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/20/2020
 */

@BindingAdapter("viewState")
fun bindMultiState(multiStateView: MultiStateView, viewState: ViewState?) {
    multiStateView.viewState = viewState ?: ViewState.EMPTY
}

@BindingAdapter("errorMessage")
fun bindMultiState(multiStateView: MultiStateView, errorMessage: String?) {
    errorMessage?.let {
        multiStateView.getView(ViewState.ERROR)
            ?.findViewWithTag<TextView>("error_message")
            ?.text = it
    }
}

@BindingAdapter("retryStateHandler")
fun bindRetryHandler(multiStateView: MultiStateView, retryHandler: MultiStateViewRetryHandler?) {
    retryHandler?.let { handler ->
        multiStateView.getView(ViewState.ERROR)
            ?.findViewWithTag<Button>("retry_action")
            ?.setOnClickListener { handler.performRetryAction() }
    }
}

/**
 * Interface callback definition to be invoked
 * when RETRY_ACTION is performed
 */
interface MultiStateViewRetryHandler {

    /**
     * Called when user attempts RETRY_ACTION
     */
    fun performRetryAction()
}