package com.nagarro.assignment.util.ext

import com.nagarro.assignment.data.Result

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/20/2020
 */

fun <T> Result<T>.onResponse(success: (T) -> Unit, error: ((Exception) -> Unit)? = null) =
    when (this) {
        is Result.Success -> success(data)
        is Result.Error -> error?.invoke(exception)
    }