package com.nagarro.assignment.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nagarro.assignment.data.AppConstant
import com.nagarro.assignment.data.Todo

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
@Database(entities = [Todo::class], version = AppConstant.TODO_DATABASE_VERSION)
abstract class TodoDatabase : RoomDatabase() {

    abstract fun getTodoDao(): TodoDao
}