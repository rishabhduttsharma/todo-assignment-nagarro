package com.nagarro.assignment.data.source.local

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo
import com.nagarro.assignment.data.source.TodoDataSource
import timber.log.Timber

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
class TodoLocalDataSource(private val todoDao: TodoDao) : TodoDataSource {

    override suspend fun getTodoItems(): Result<List<Todo>> = try {
        Result.Success(todoDao.getTodoItems())
    } catch (ex: Exception) {
        Timber.e(ex)
        Result.Error(ex)
    }

    override suspend fun saveTodoItem(todoItem: Todo) =
        todoDao.insertTodoItems(todoItem)
}