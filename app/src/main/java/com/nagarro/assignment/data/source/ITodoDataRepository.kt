package com.nagarro.assignment.data.source

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo


/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
interface ITodoDataRepository {

    /**
     * Retrieves items from Web Api Server
     */
    suspend fun getTodoItems(): Result<List<Todo>>


    /**
     * Retrieves items from Local Database as per [forceUpdate]
     *
     * @param forceUpdate updates the local database before retrieving, if true
     */
    suspend fun refreshTodoItems(forceUpdate: Boolean = false): Result<List<Todo>>
}