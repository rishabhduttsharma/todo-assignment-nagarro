package com.nagarro.assignment.data.source.remote.api

import com.nagarro.assignment.data.Todo
import com.nagarro.assignment.data.source.remote.Api
import retrofit2.http.GET

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
interface TodoApi {

    @GET(Api.GET_TODOS)
    suspend fun getTodoItems(): List<Todo>
}