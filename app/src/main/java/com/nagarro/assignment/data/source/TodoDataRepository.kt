package com.nagarro.assignment.data.source

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo
import timber.log.Timber

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
class TodoDataRepository(
    private val localDataSource: TodoDataSource,
    private val remoteTodoDataSource: TodoDataSource
) : ITodoDataRepository {

    override suspend fun getTodoItems() = remoteTodoDataSource.getTodoItems()

    override suspend fun refreshTodoItems(forceUpdate: Boolean): Result<List<Todo>> {
        if (forceUpdate) try {
            when (val todoResult = remoteTodoDataSource.getTodoItems()) {
                is Result.Success -> {
                    // save to-do-items
                    todoResult.data.forEach { todoItem ->
                        localDataSource.saveTodoItem(todoItem)
                    }
                }
                is Result.Error -> {
                    throw todoResult.exception
                }
            }
        } catch (ex: Exception) {
            Timber.e(ex)
        }
        return localDataSource.getTodoItems()
    }
}