package com.nagarro.assignment.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
@Entity(tableName = AppConstant.RoomEntityName.TODO)
data class Todo(

    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int? = null,

    @ColumnInfo(name = "userId")
    val userId: Int? = null,

    @ColumnInfo(name = "title")
    val title: String? = null,

    @ColumnInfo(name = "completed")
    val completed: Boolean? = null
)