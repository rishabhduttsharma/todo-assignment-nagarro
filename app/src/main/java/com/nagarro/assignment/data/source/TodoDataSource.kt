package com.nagarro.assignment.data.source

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
interface TodoDataSource {

    suspend fun getTodoItems(): Result<List<Todo>>

    suspend fun saveTodoItem(todoItem: Todo)
}