package com.nagarro.assignment.data.source.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.nagarro.assignment.data.AppConstant
import com.nagarro.assignment.data.Todo

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/13/2020
 */
@Dao
interface TodoDao {

    // queries
    @Query("SELECT * FROM ${AppConstant.RoomEntityName.TODO}")
    suspend fun getTodoItems(): List<Todo>

    // observable queries
    @Query("SELECT * FROM ${AppConstant.RoomEntityName.TODO}")
    fun observeTodoItems(): LiveData<List<Todo>>

    // insert to-do-items
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodoItems(todo: Todo)
}