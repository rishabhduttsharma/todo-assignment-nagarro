package com.nagarro.assignment.data

import androidx.annotation.StringDef

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/18/2020
 */
object AppConstant {

    const val TODO_DATABASE_VERSION = 1

    @StringDef(RoomEntityName.TODO)
    annotation class RoomEntityName {
        companion object {
            const val TODO = "todo"
        }
    }

    @StringDef(RoomDatabaseName.TODO)
    annotation class RoomDatabaseName {
        companion object {
            const val TODO = "todo.db"
        }
    }
}