package com.nagarro.assignment.data.source.remote

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo
import com.nagarro.assignment.data.source.TodoDataSource
import com.nagarro.assignment.data.source.remote.api.TodoApi
import com.nagarro.assignment.util.ext.toErrorResult
import timber.log.Timber

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/17/2020
 */
class TodoRemoteDataSource(private val todoApi: TodoApi) : TodoDataSource {

    override suspend fun getTodoItems(): Result<List<Todo>> = try {
        // attempt web-api-call, and map to success-result
        Result.Success(todoApi.getTodoItems())
    } catch (ex: Exception) {
        with(ex) {
            // log error
            Timber.e(this)
            // map to error-result
            toErrorResult()
        }
    }

    override suspend fun saveTodoItem(todoItem: Todo) =
        throw NotImplementedError("Api not available!")
}