package com.nagarro.assignment.ui.todolist

import android.os.Bundle
import com.nagarro.assignment.R
import com.nagarro.assignment.databinding.FragmentTodoListBinding
import com.nagarro.assignment.ui.BaseFragment

class TodoListFragment : BaseFragment<FragmentTodoListBinding>(R.layout.fragment_todo_list) {

    override fun initComponents(savedInstanceState: Bundle?, binding: FragmentTodoListBinding) {
        // retrieve view-model
        getViewModel<TodoListViewModel>().apply {
            binding.viewModel = this
        }.loadTodoList()
    }
}
