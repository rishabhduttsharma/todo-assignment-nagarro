package com.nagarro.assignment.ui.todolist

import android.os.Bundle
import androidx.fragment.app.commitNow
import com.nagarro.assignment.R
import com.nagarro.assignment.databinding.MainActivityBinding
import com.nagarro.assignment.ui.BaseActivity

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/17/2020
 */
class TodoListActivity : BaseActivity<MainActivityBinding>(R.layout.main_activity) {

    override fun initComponents(savedInstanceState: Bundle?, binding: MainActivityBinding) {
        savedInstanceState ?: supportFragmentManager.commitNow {
            replace(R.id.container, TodoListFragment())
        }
    }
}