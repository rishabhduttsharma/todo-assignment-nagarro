package com.nagarro.assignment.ui.todolist

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.kennyc.view.MultiStateView

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/20/2020
 */
class TodoListBinder : BaseObservable() {

    @get:Bindable
    var todoListViewState = MultiStateView.ViewState.EMPTY
        set(value) {
            field = value
            notifyPropertyChanged(BR.todoListViewState)
        }

    @get:Bindable
    var errorMessage: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.errorMessage)
        }
}