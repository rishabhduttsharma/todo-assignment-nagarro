package com.nagarro.assignment.ui

import androidx.lifecycle.ViewModel

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/17/2020
 */
open class BaseViewModel : ViewModel()