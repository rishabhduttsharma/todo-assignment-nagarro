package com.nagarro.assignment.ui.todolist

import androidx.databinding.ObservableArrayList
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.viewModelScope
import com.kennyc.view.MultiStateView
import com.nagarro.assignment.R
import com.nagarro.assignment.data.Todo
import com.nagarro.assignment.data.source.ITodoDataRepository
import com.nagarro.assignment.ui.BaseViewModel
import com.nagarro.assignment.util.bindingadapter.MultiStateViewRetryHandler
import com.nagarro.assignment.util.ext.onResponse
import kotlinx.coroutines.launch
import me.tatarka.bindingcollectionadapter2.ItemBinding
import timber.log.Timber
import javax.inject.Inject

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/17/2020
 */
class TodoListViewModel @Inject constructor(
    private val repository: ITodoDataRepository
) : BaseViewModel(), MultiStateViewRetryHandler {

    val todoBinder = TodoListBinder()

    val todoItems = ObservableArrayList<Todo>()
    val todoItemsBinding: ItemBinding<Todo> = ItemBinding.of<Todo>(
        BR.todoItem, R.layout.adapter_todo_item
    )

    internal fun loadTodoList() {
        // launch co-routine
        viewModelScope.launch {
            // show loading view
            todoBinder.todoListViewState = MultiStateView.ViewState.LOADING
            // call web-api
            repository.getTodoItems().onResponse(::handleResult, ::handleError)
        }
    }

    private fun handleResult(items: List<Todo>) {
        // replace existing items
        todoItems.clear()
        todoItems.addAll(items)
        // show content view
        todoBinder.todoListViewState = MultiStateView.ViewState.CONTENT
    }

    private fun handleError(exception: Exception) {
        Timber.e(exception)
        // display error
        todoBinder.errorMessage = exception.message
        // show error view
        todoBinder.todoListViewState = MultiStateView.ViewState.ERROR
    }

    override fun performRetryAction() = loadTodoList().also {
        Timber.e("RETRY_ACTION performed")
    }
}
