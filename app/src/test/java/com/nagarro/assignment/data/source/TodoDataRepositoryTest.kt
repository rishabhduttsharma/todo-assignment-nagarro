package com.nagarro.assignment.data.source

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

/**
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/21/2020
 */
@ExperimentalCoroutinesApi
class TodoDataRepositoryTest {

    private val fakeTodoItems = listOf(
        Todo(1, 100, "Sample Title 1", true),
        Todo(2, 100, "Sample Title 2", false),
        Todo(3, 100, "Sample Title 3", true)
    )

    private lateinit var todoDataRepository: ITodoDataRepository

    @Before
    fun initializeRepository() {
        val fakeDataSource = FakeDataSource(fakeTodoItems)
        todoDataRepository = TodoDataRepository(
            remoteTodoDataSource = fakeDataSource,
            localDataSource = fakeDataSource
        )
    }

    @Test
    fun getTodoItems_requestTodoItemsFromRemoteDataSource_resultSuccess() = runBlockingTest {
        val result = todoDataRepository.getTodoItems()
        assertThat(result, instanceOf(Result.Success::class.java))
    }

    @Test
    fun getTodoItems_requestTodoItemsFromRemoteDataSource_returnsFakeItems() = runBlockingTest {
        val result = todoDataRepository.getTodoItems() as Result.Success
        assertThat(result.data, `is`(fakeTodoItems))
    }
}