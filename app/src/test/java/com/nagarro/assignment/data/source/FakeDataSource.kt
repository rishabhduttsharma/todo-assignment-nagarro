package com.nagarro.assignment.data.source

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/21/2020
 */
class FakeDataSource(private val fakeTodoItems: List<Todo>) : TodoDataSource {

    override suspend fun getTodoItems(): Result<List<Todo>> = Result.Success(fakeTodoItems)

    override suspend fun saveTodoItem(todoItem: Todo) = TODO("not implemented")
}