package com.nagarro.assignment.ui.todolist

import com.nagarro.assignment.data.Result
import com.nagarro.assignment.data.Todo
import com.nagarro.assignment.data.source.ITodoDataRepository

/**
 *
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/20/2020
 */
class FakeTodoDataRepository : ITodoDataRepository {

    override suspend fun getTodoItems(): Result<List<Todo>> = listOf(
        Todo(1, 1, "Sample title", true)
    ).let { Result.Success(it) }

    override suspend fun refreshTodoItems(forceUpdate: Boolean): Result<List<Todo>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}