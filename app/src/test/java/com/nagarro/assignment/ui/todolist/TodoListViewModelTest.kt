package com.nagarro.assignment.ui.todolist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Developer: Rishabh Dutt Sharma
 * Dated: 2/20/2020
 */
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class TodoListViewModelTest {

    private lateinit var viewModel: TodoListViewModel

    // synchronous execution of tasks
    @get:Rule
    val executorRule = InstantTaskExecutorRule()

    @Before
    fun setupViewModel() {
        // given
        viewModel = TodoListViewModel(FakeTodoDataRepository())
    }

    @Test
    fun loadTodoList_checkHandleResult() = runBlockingTest {
        // when
        viewModel.loadTodoList()
        // then
        assertThat(viewModel.todoItems.size, not(0))
    }
}